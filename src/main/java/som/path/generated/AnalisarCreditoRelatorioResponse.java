
package som.path.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AnalisarCreditoRelatorioResult" type="{https://sitenet05.serasa.com.br/wsgestordecisao/wsgestordecisao}AnalisarCreditoRelatorioRetorno" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarCreditoRelatorioResult"
})
@XmlRootElement(name = "AnalisarCreditoRelatorioResponse")
public class AnalisarCreditoRelatorioResponse {

    @XmlElement(name = "AnalisarCreditoRelatorioResult")
    protected AnalisarCreditoRelatorioRetorno analisarCreditoRelatorioResult;

    /**
     * Obt�m o valor da propriedade analisarCreditoRelatorioResult.
     * 
     * @return
     *     possible object is
     *     {@link AnalisarCreditoRelatorioRetorno }
     *     
     */
    public AnalisarCreditoRelatorioRetorno getAnalisarCreditoRelatorioResult() {
        return analisarCreditoRelatorioResult;
    }

    /**
     * Define o valor da propriedade analisarCreditoRelatorioResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AnalisarCreditoRelatorioRetorno }
     *     
     */
    public void setAnalisarCreditoRelatorioResult(AnalisarCreditoRelatorioRetorno value) {
        this.analisarCreditoRelatorioResult = value;
    }

    public AnalisarCreditoRelatorioResponse withAnalisarCreditoRelatorioResult(AnalisarCreditoRelatorioRetorno value) {
        setAnalisarCreditoRelatorioResult(value);
        return this;
    }

}
