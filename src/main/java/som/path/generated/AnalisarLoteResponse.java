
package som.path.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AnalisarLoteResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarLoteResult"
})
@XmlRootElement(name = "AnalisarLoteResponse")
public class AnalisarLoteResponse {

    @XmlElement(name = "AnalisarLoteResult")
    protected String analisarLoteResult;

    /**
     * Obt�m o valor da propriedade analisarLoteResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalisarLoteResult() {
        return analisarLoteResult;
    }

    /**
     * Define o valor da propriedade analisarLoteResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalisarLoteResult(String value) {
        this.analisarLoteResult = value;
    }

    public AnalisarLoteResponse withAnalisarLoteResult(String value) {
        setAnalisarLoteResult(value);
        return this;
    }

}
