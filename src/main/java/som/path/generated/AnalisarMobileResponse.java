
package som.path.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AnalisarMobileResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarMobileResult"
})
@XmlRootElement(name = "AnalisarMobileResponse")
public class AnalisarMobileResponse {

    @XmlElement(name = "AnalisarMobileResult")
    protected String analisarMobileResult;

    /**
     * Obt�m o valor da propriedade analisarMobileResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalisarMobileResult() {
        return analisarMobileResult;
    }

    /**
     * Define o valor da propriedade analisarMobileResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalisarMobileResult(String value) {
        this.analisarMobileResult = value;
    }

    public AnalisarMobileResponse withAnalisarMobileResult(String value) {
        setAnalisarMobileResult(value);
        return this;
    }

}
