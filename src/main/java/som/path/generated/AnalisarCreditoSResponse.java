
package som.path.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AnalisarCreditoSResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "analisarCreditoSResult"
})
@XmlRootElement(name = "AnalisarCreditoSResponse")
public class AnalisarCreditoSResponse {

    @XmlElement(name = "AnalisarCreditoSResult")
    protected String analisarCreditoSResult;

    /**
     * Obt�m o valor da propriedade analisarCreditoSResult.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnalisarCreditoSResult() {
        return analisarCreditoSResult;
    }

    /**
     * Define o valor da propriedade analisarCreditoSResult.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnalisarCreditoSResult(String value) {
        this.analisarCreditoSResult = value;
    }

    public AnalisarCreditoSResponse withAnalisarCreditoSResult(String value) {
        setAnalisarCreditoSResult(value);
        return this;
    }

}
