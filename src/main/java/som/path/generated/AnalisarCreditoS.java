
package som.path.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de anonymous complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sCNPJ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sUsrGC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sPassGC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sUsrSer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sPassSer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VrCompra" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="sScore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bSerasa" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="bAtualizar" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="sOnLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sSociosCNPJ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sSociosCPF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sIdSocios" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="iQuadroSocial" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "scnpj",
    "sUsrGC",
    "sPassGC",
    "sUsrSer",
    "sPassSer",
    "sDoc",
    "vrCompra",
    "sScore",
    "bSerasa",
    "bAtualizar",
    "sOnLine",
    "sSociosCNPJ",
    "sSociosCPF",
    "sIdSocios",
    "iQuadroSocial"
})
@XmlRootElement(name = "AnalisarCreditoS")
public class AnalisarCreditoS {

    @XmlElement(name = "sCNPJ")
    protected String scnpj;
    protected String sUsrGC;
    protected String sPassGC;
    protected String sUsrSer;
    protected String sPassSer;
    protected String sDoc;
    @XmlElement(name = "VrCompra")
    protected double vrCompra;
    protected String sScore;
    protected boolean bSerasa;
    protected boolean bAtualizar;
    protected String sOnLine;
    protected String sSociosCNPJ;
    protected String sSociosCPF;
    protected String sIdSocios;
    protected int iQuadroSocial;

    /**
     * Obt�m o valor da propriedade scnpj.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSCNPJ() {
        return scnpj;
    }

    /**
     * Define o valor da propriedade scnpj.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSCNPJ(String value) {
        this.scnpj = value;
    }

    /**
     * Obt�m o valor da propriedade sUsrGC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUsrGC() {
        return sUsrGC;
    }

    /**
     * Define o valor da propriedade sUsrGC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUsrGC(String value) {
        this.sUsrGC = value;
    }

    /**
     * Obt�m o valor da propriedade sPassGC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPassGC() {
        return sPassGC;
    }

    /**
     * Define o valor da propriedade sPassGC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPassGC(String value) {
        this.sPassGC = value;
    }

    /**
     * Obt�m o valor da propriedade sUsrSer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSUsrSer() {
        return sUsrSer;
    }

    /**
     * Define o valor da propriedade sUsrSer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSUsrSer(String value) {
        this.sUsrSer = value;
    }

    /**
     * Obt�m o valor da propriedade sPassSer.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPassSer() {
        return sPassSer;
    }

    /**
     * Define o valor da propriedade sPassSer.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPassSer(String value) {
        this.sPassSer = value;
    }

    /**
     * Obt�m o valor da propriedade sDoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSDoc() {
        return sDoc;
    }

    /**
     * Define o valor da propriedade sDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSDoc(String value) {
        this.sDoc = value;
    }

    /**
     * Obt�m o valor da propriedade vrCompra.
     * 
     */
    public double getVrCompra() {
        return vrCompra;
    }

    /**
     * Define o valor da propriedade vrCompra.
     * 
     */
    public void setVrCompra(double value) {
        this.vrCompra = value;
    }

    /**
     * Obt�m o valor da propriedade sScore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSScore() {
        return sScore;
    }

    /**
     * Define o valor da propriedade sScore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSScore(String value) {
        this.sScore = value;
    }

    /**
     * Obt�m o valor da propriedade bSerasa.
     * 
     */
    public boolean isBSerasa() {
        return bSerasa;
    }

    /**
     * Define o valor da propriedade bSerasa.
     * 
     */
    public void setBSerasa(boolean value) {
        this.bSerasa = value;
    }

    /**
     * Obt�m o valor da propriedade bAtualizar.
     * 
     */
    public boolean isBAtualizar() {
        return bAtualizar;
    }

    /**
     * Define o valor da propriedade bAtualizar.
     * 
     */
    public void setBAtualizar(boolean value) {
        this.bAtualizar = value;
    }

    /**
     * Obt�m o valor da propriedade sOnLine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSOnLine() {
        return sOnLine;
    }

    /**
     * Define o valor da propriedade sOnLine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSOnLine(String value) {
        this.sOnLine = value;
    }

    /**
     * Obt�m o valor da propriedade sSociosCNPJ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSociosCNPJ() {
        return sSociosCNPJ;
    }

    /**
     * Define o valor da propriedade sSociosCNPJ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSociosCNPJ(String value) {
        this.sSociosCNPJ = value;
    }

    /**
     * Obt�m o valor da propriedade sSociosCPF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSociosCPF() {
        return sSociosCPF;
    }

    /**
     * Define o valor da propriedade sSociosCPF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSociosCPF(String value) {
        this.sSociosCPF = value;
    }

    /**
     * Obt�m o valor da propriedade sIdSocios.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSIdSocios() {
        return sIdSocios;
    }

    /**
     * Define o valor da propriedade sIdSocios.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSIdSocios(String value) {
        this.sIdSocios = value;
    }

    /**
     * Obt�m o valor da propriedade iQuadroSocial.
     * 
     */
    public int getIQuadroSocial() {
        return iQuadroSocial;
    }

    /**
     * Define o valor da propriedade iQuadroSocial.
     * 
     */
    public void setIQuadroSocial(int value) {
        this.iQuadroSocial = value;
    }

    public AnalisarCreditoS withSCNPJ(String value) {
        setSCNPJ(value);
        return this;
    }

    public AnalisarCreditoS withSUsrGC(String value) {
        setSUsrGC(value);
        return this;
    }

    public AnalisarCreditoS withSPassGC(String value) {
        setSPassGC(value);
        return this;
    }

    public AnalisarCreditoS withSUsrSer(String value) {
        setSUsrSer(value);
        return this;
    }

    public AnalisarCreditoS withSPassSer(String value) {
        setSPassSer(value);
        return this;
    }

    public AnalisarCreditoS withSDoc(String value) {
        setSDoc(value);
        return this;
    }

    public AnalisarCreditoS withVrCompra(double value) {
        setVrCompra(value);
        return this;
    }

    public AnalisarCreditoS withSScore(String value) {
        setSScore(value);
        return this;
    }

    public AnalisarCreditoS withBSerasa(boolean value) {
        setBSerasa(value);
        return this;
    }

    public AnalisarCreditoS withBAtualizar(boolean value) {
        setBAtualizar(value);
        return this;
    }

    public AnalisarCreditoS withSOnLine(String value) {
        setSOnLine(value);
        return this;
    }

    public AnalisarCreditoS withSSociosCNPJ(String value) {
        setSSociosCNPJ(value);
        return this;
    }

    public AnalisarCreditoS withSSociosCPF(String value) {
        setSSociosCPF(value);
        return this;
    }

    public AnalisarCreditoS withSIdSocios(String value) {
        setSIdSocios(value);
        return this;
    }

    public AnalisarCreditoS withIQuadroSocial(int value) {
        setIQuadroSocial(value);
        return this;
    }

}
