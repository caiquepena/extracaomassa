package com.serasa.test.soap;

import java.text.DecimalFormat;

import org.junit.runner.JUnitCore;

import com.serasa.test.utils.ContadorExcel;

public class Start {
	ContadorExcel c;
	static String nomeAba;
	static int linhas;
	static DecimalFormat df = new DecimalFormat("0");
	
	public static void main(String[] args) throws Exception {
		ContadorExcel c = new ContadorExcel();
		String politica = System.getProperty("politica");
		getNomeAba(politica);
		linhas = c.count(nomeAba);

		for (int i = 0; linhas > i; i++) {
			System.out.println(">>>>>>>>>  SERÃO EXTRAIDOS: " + linhas + " dados <<<<<<<<<<");
			System.out
					.println(">>>>>>>>>  TEMPO APROXIMADO DE : " + (df.format((((linhas * 2.5) / 60) / 60))) + " HORAS <<<<<<<<<<");
			System.out.println("///////////////////////////////////////////////////////////////");
			run(politica);
			
		}
	}

	public static void run(String politica) throws Exception {
		if (politica.equals("tipoconsulta@1")) {
			System.out.println("Relato");
			JUnitCore.main("com.serasa.test.soap.ExtracaoGestorSoapRelato");
		}
		if (politica.equals("tipoconsulta@2")) {
			System.out.println("Risk");
			JUnitCore.main("com.serasa.test.soap.ExtracaoGestorSoapRisk");
		}
		if (politica.equals("tipoconsulta@3")) {
			System.out.println("Rating");
			JUnitCore.main("com.serasa.test.soap.ExtracaoGestorSoapRating");
		}
		if (politica.equals("tipoconsulta@4")) {
			System.out.println("CPF");
			JUnitCore.main("com.serasa.test.soap.ExtracaoGestorSoapCpfCSBA");
		} 
		if (politica.equals(null)) {
			System.out.println("Nenhum condição para :"+politica);
		}

	}

	public static String getNomeAba(String politica) {
		if (politica.equals("tipoconsulta@1")) {
			nomeAba = "ExtracaoGestorSoapRelato";
		}
		if (politica.equals("tipoconsulta@2")) {
			nomeAba = "ExtracaoGestorSoapRisk";
		}
		if (politica.equals("tipoconsulta@3")) {
			nomeAba = "ExtracaoGestorSoapRating";
		}
		if (politica.equals("tipoconsulta@4")) {
			nomeAba = "ExtracaoGestorSoapCpf";
		}
		return nomeAba;
	}
}