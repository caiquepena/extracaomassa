package com.serasa.test.soap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataDictionary;
import com.serasa.test.utils.AutomationSoap;
import com.serasa.test.utils.ContadorExcel;
import com.serasa.test.utils.GestorXLSXSoapRating;

import som.path.generated.WsgestordecisaoSoapTest;

@RunWith(Parameterized.class)
public class ExtracaoGestorSoapRating extends AutomationSoap {

	String mensaje = "";
	String nameProject;
	GestorXLSXSoapRating writeXlsxRating;

	Map<String, String> dadosRating = new HashMap<String, String>();

	// Pages
	WsgestordecisaoSoapTest pageSoap;
	ContadorExcel c;
	

	public ExtracaoGestorSoapRating(String executionName, DataDictionary data) {
		ExecutionInfo.setExecutionName(executionName);
		GlobalData.load(data);
	}

	@Parameters(name = "{0}")
	public static List<Object> loadTestData() throws Exception {
		System.setProperty("inputType", "EXCEL");
		System.setProperty("platform", "DESKTOP-JAVA");
		return loadData();
	}
 
	@Before
	public void beforeTest() throws Exception {
		pageSoap = new WsgestordecisaoSoapTest();
		writeXlsxRating = new GestorXLSXSoapRating();
		c = new ContadorExcel();
	}

	@Test
	public void script() throws Exception {
		newSoapCreditoRiskScoring();
	}

	// **********************
	// * Funções auxiliares *
	// **********************
	
	
	public void newSoapCreditoRiskScoring() throws Exception {
		pageSoap.getDados(GlobalData.getData("vCNPJ"), 3);
		pageSoap.splitString1();
		dadosRating = pageSoap.dadosSoap();
		writeXlsxRating.addDadosXSLX(dadosRating);
		ExecutionInfo.setResult("EXTRAIDO");
		Thread.sleep(400);
		c.count("ExtracaoGestorSoapRating");
	}
}
