package com.serasa.test.soap;

import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataDictionary;
import com.serasa.test.utils.AutomationSoap;
import com.serasa.test.utils.ContadorExcel;
import com.serasa.test.utils.GestorXLSXSoapYahsatPJ;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import som.path.generated.WsgestordecisaoSoapTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(Parameterized.class)
public class ExtracaoGestorSoapYahsatPJ extends AutomationSoap {

    String mensaje = "";
    String nameProject;
    GestorXLSXSoapYahsatPJ writeXLSXCnpjYahsat;

    Map<String, String> dadosCSBA = new HashMap<String, String>();

    // Pages
    WsgestordecisaoSoapTest pageSoap;
    ContadorExcel c;


    public ExtracaoGestorSoapYahsatPJ(String executionName, DataDictionary data) {
        ExecutionInfo.setExecutionName(executionName);
        GlobalData.load(data);
    }

    @Parameters(name = "{0}")
    public static List<Object> loadTestData() throws Exception {
        System.setProperty("inputType", "EXCEL");
        System.setProperty("platform", "DESKTOP-JAVA");
        return loadData();
    }


    @Before
    public void beforeTest() throws Exception {
        pageSoap = new WsgestordecisaoSoapTest();
        writeXLSXCnpjYahsat = new GestorXLSXSoapYahsatPJ();
        c = new ContadorExcel();
    }

    @Test
    public void script() throws Exception {
        newSoapCreditoYahsatPJ();
    }

    // **********************
    // * Funções auxiliares *
    // **********************

    public void newSoapCreditoYahsatPJ() throws Exception {
        pageSoap.getDadosYahsatPJ(GlobalData.getData("cnpj"));
        //pageSoap.splitString1();
        //dadosCSBA = pageSoap.dadosSoap();
        //writeXLSXCpfYahsat.addDadosXSLX(dadosCSBA);
        ExecutionInfo.setResult("EXTRAIDO");
        Thread.sleep(500);
        c.count("ExtracaoGestorSoapYahsatPJ");
    }
}
