package com.serasa.test.soap;

import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataDictionary;
import com.serasa.test.utils.AutomationSoap;
import com.serasa.test.utils.ContadorExcel;
import com.serasa.test.utils.GestorXLSXSoapCanonPJ;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import som.path.generated.WsgestordecisaoSoapTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(Parameterized.class)
public class ExtracaoGestorSoapCanonPJ extends AutomationSoap {

    String mensaje = "";
    String nameProject;
    GestorXLSXSoapCanonPJ writeXlsxCanonPJ;

    Map<String, String> dadosCanonPJ = new HashMap<String, String>();


    // Pages
    WsgestordecisaoSoapTest pageSoap;
    ContadorExcel c;


    public ExtracaoGestorSoapCanonPJ(String executionName, DataDictionary data) {
        ExecutionInfo.setExecutionName(executionName);
        GlobalData.load(data);
    }

    @Parameters(name = "{0}")
    public static List<Object> loadTestData() throws Exception {
        System.setProperty("inputType", "EXCEL");
        System.setProperty("platform", "DESKTOP-JAVA");
        return loadData();
    }

    @Before
    public void beforeTest() throws Exception {
        pageSoap = new WsgestordecisaoSoapTest();
        writeXlsxCanonPJ = new GestorXLSXSoapCanonPJ();
        c = new ContadorExcel();
    }

    @Test
    public void script() throws Exception {
        newSoapCreditoCanonPJ();
    }


    // **********************
    // * Funções auxiliares *
    // **********************


    public void newSoapCreditoCanonPJ() throws Exception {
        pageSoap.getDadosCanonPJ(GlobalData.getData("vCNPJ"));
        //pageSoap.splitString1();
        //dadosCanonPJ = pageSoap.dadosSoap();
        //writeXlsxCanonPJ.addDadosXSLX(dadosCanonPJ);
        ExecutionInfo.setResult("EXTRAIDO");
        Thread.sleep(500);
        c.count("ExtracaoGestorSoapCanonPJ");
    }
}
