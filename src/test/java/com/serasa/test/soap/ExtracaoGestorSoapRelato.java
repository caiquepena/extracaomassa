package com.serasa.test.soap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.everis.ExecutionInfo;
import com.everis.GlobalData;
import com.everis.data.DataDictionary;
import com.serasa.test.utils.AutomationSoap;
import com.serasa.test.utils.ContadorExcel;
import com.serasa.test.utils.GestorXLSXSoapRelato;

import som.path.generated.WsgestordecisaoSoapTest;

@RunWith(Parameterized.class)
public class ExtracaoGestorSoapRelato extends AutomationSoap {

    String mensaje = "";
    String nameProject;
    GestorXLSXSoapRelato writeXlsxRelato;

    Map<String, String> dadosRelato = new HashMap<String, String>();

    // Pages
    WsgestordecisaoSoapTest pageSoap;
    ContadorExcel c;


    public ExtracaoGestorSoapRelato(String executionName, DataDictionary data) {
        ExecutionInfo.setExecutionName(executionName);
        GlobalData.load(data);
    }

    @Parameters(name = "{0}")
    public static List<Object> loadTestData() throws Exception {
        System.setProperty("inputType", "EXCEL");
        System.setProperty("platform", "DESKTOP-JAVA");
        return loadData();
    }


    @Before
    public void beforeTest() throws Exception {
        pageSoap = new WsgestordecisaoSoapTest();
        writeXlsxRelato = new GestorXLSXSoapRelato();
        c = new ContadorExcel();
    }

    @Test
    public void script() throws Exception {
        newSoapCreditoRelato();
    }

    // **********************
    // * Funções auxiliares *
    // **********************

    public void newSoapCreditoRelato() throws Exception {
        pageSoap.getDados(GlobalData.getData("vCNPJ"), 1);
        pageSoap.splitString1();
        dadosRelato = pageSoap.dadosSoap();
        writeXlsxRelato.addDadosXSLX(dadosRelato);
        ExecutionInfo.setResult("EXTRAIDO");
        Thread.sleep(500);
        c.count("ExtracaoGestorSoapRelato");
    }
}
