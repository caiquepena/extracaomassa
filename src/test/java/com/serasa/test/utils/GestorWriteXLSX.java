package com.serasa.test.utils;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.impl.xb.xsdschema.ListDocument.List;

import com.everis.GlobalData;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.poi.*;

/**
 * EXPORT XMLS
 * 
 * @author cxe1813
 *
 */
public class GestorWriteXLSX {
	String pathXLS = "./massaDados/Relatório GESTOR.xls";
	String pathXLSX = "./massaDados/Relatório GESTOR.xlsx";
	int i = 0;
	int rownum = 0;
	int cellnum = 0;
	Cell cell;
	Row row;
	Map<String, String> dadosResumoDecisao = new HashMap<String, String>();

	public void addDadosXSLX(Map map) throws IOException {
		dadosResumoDecisao = map;
		cellnum = 0;
		Cell cell2;
		FileInputStream file = new FileInputStream(new File(pathXLS));
		HSSFWorkbook workbook = new HSSFWorkbook(file);
		HSSFSheet sheetPCOD = workbook.getSheet("PCOD");
		
		rownum = sheetPCOD.getPhysicalNumberOfRows();
				//Integer.parseInt(GlobalData.getData("vLinha"));
		
		try {

			row = sheetPCOD.createRow(rownum); // linha
			cell2 = row.createCell(cellnum++);// Coluna 
			cell2.setCellValue((String) GlobalData.getData("vCNPJ"));

			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("STATUS DO CNPJ"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("EXTINCAO DE FALENCIA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FALENCIA DECRETADA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FALENCIA REQUERIDA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("AUTO-FALENCIA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("CONCORDATA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("CONCORDATA DEFERIDA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("CONCORDATA REQUERIDA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("CONCORDATA SUSPENSIVA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES JUDICIAIS ATE A DATA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("ALERTA (S / N)"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT PEFIN TELECOM ATE A DATA"));
				
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VR PEFIN TELECOM ATE A DATA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT PEFIN-EMPRESAS ATE A DATA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PONTUALIDADE_PAGAMENTO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PROTESTO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PEFIN"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("SUGESTAO_LIMITE_MERCADO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("BASE_LIMITE_CONCEDIDO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RESTRITIVO_SOCIOS"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("SOCIO_1"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("SOCIO_2"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("SOCIO_3"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("CAPITAL_SOCIAL"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("ADM_1"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPADA_1"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPADA_2"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPADA_3"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPANTE_1"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPANTE_2"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPANTE_3"));

			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("NUMERO_FUNCIONARIOS"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("ERRO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_1"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_2"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_3"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_4"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_5"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_6"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_7"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_8"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_9"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_10"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_REPROVACAO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("MOTIVO_RETAGUARDA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("CLASSIFICACAO_INTERNA"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FATOR_LIMITE_CREDITO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FATOR_MAJORADOR_LIMTE_CREDITO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PERIODICIDADE_REVALIDACAO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("LIMITE_CREDITO_CONCEDIDO"));
			
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("LIMITE_CREDITO_CALCULADO"));
			
			file.close();

			FileOutputStream outFile = new FileOutputStream(new File(pathXLS));
			workbook.write(outFile);
			outFile.close();
			System.out.println("Arquivo Excel editado com sucesso!");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo Excel não encontrado!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro na edição do arquivo!");
		}
	}
	
	public void criateXLSX() {
		  HSSFWorkbook workbook = new HSSFWorkbook();
          HSSFSheet sheetPCOD = workbook.createSheet("PCOD");

        row = sheetPCOD.createRow(rownum++);
  		cell = row.createCell(cellnum++);
  		cell.setCellValue("CNPJ");

  		cell = row.createCell(cellnum++);
  		cell.setCellValue("DECISÃO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("VALOR_PARCELA");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("VALOR_SOLICITADO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("RAZAO_SOCIAL");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("DATA_DE_FUNDACAO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("STATUS_CNPJ");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("SINTEGRA");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("RECEITA_FEDERAL");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("RELATO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("SCORE_DE_CREDITO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("RISCO_EXTERNO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("CLASSIFICACAO_ALTISSIMO_RISCO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PORTE");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("VALOR_LIMITE");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PONTUALIDADE_PAGAMENTO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PROTESTO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PEFIN");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("SUGESTAO_LIMITE_MERCADO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("BASE_LIMITE_CONCEDIDO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("RESTRITIVO_SOCIOS");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("SOCIO_1");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("SOCIO_2");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("SOCIO_3");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("CAPITAL_SOCIAL");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("ADM_1");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PARTICIPADA_1");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PARTICIPADA_2");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PARTICIPADA_3");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PARTICIPANTE_1");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PARTICIPANTE_2");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PARTICIPANTE_3");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("NUMERO_FUNCIONARIOS");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("ERRO");
  		
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_1");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_2");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_3");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_4");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_5");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_6");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_7");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_8");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_9");
		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_10");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("MOTIVO_REPROVACAO");
  		
 		cell = row.createCell(cellnum++);		
  		cell.setCellValue("MOTIVO_RETAGUARDA");
  		
 		cell = row.createCell(cellnum++); 		
  		cell.setCellValue("CLASSIFICACAO_INTERNA");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("FATOR_LIMITE_CREDITO");
  		
 		cell = row.createCell(cellnum++);
 		cell.setCellValue("FATOR_MAJORADOR_LIMTE_CREDITO");
 		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("PERIODICIDADE_REVALIDACAO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("LIMITE_CREDITO_CONCEDIDO");
  		
 		cell = row.createCell(cellnum++);
  		cell.setCellValue("LIMITE_CREDITO_CALCULADO");
  		
  		
  		
          try {
              FileOutputStream out = new FileOutputStream(new File(pathXLS));
              workbook.write(out);
              out.close();
              System.out.println("Arquivo Excel criado com sucesso!");
                
          } catch (FileNotFoundException e) {
              e.printStackTrace();
                 System.out.println("Arquivo não encontrado!");
          } catch (IOException e) {
              e.printStackTrace();
                 System.out.println("Erro na edição do arquivo!");
          }
    }
	
}
