package com.serasa.test.utils;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.serasa.test.soap.Start;

public class ContadorExcel {
	static int rowNum;
	public int count(String nomeAba) throws Exception {
		FileInputStream fis = new FileInputStream("./data/soap.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet(nomeAba);
		XSSFRow row = sheet.getRow(0);
		rowNum = sheet.getLastRowNum() + 1;

//		for (int i = 0; rowNum > i; i++) {
//			System.out.println(">>>>>>>>>EXTRAIDO " + i + " DE " + rowNum + "<<<<<<<<<<");
//			System.out.println("///////////////////////////////////////////////////////////////");
//		}
		
		return rowNum;
	}
	
}
