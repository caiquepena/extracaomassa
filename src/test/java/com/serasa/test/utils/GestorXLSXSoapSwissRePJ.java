package com.serasa.test.utils;

import com.everis.GlobalData;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * EXPORT XMLS
 *
 * @author cfm2748
 *
 */


public class GestorXLSXSoapSwissRePJ {
	String pathXLS = "./massaDados/Relatório GESTOR Soap *(NOMEDOPROJETO)*.xls";
	int i = 0;
	int rownum = 0;
	int cellnum = 0;
	Cell cell;
	Row row;
	Map<String, String> dadosResumoDecisao = new HashMap<String, String>();

	public void addDadosXSLX(Map map) throws IOException {
		dadosResumoDecisao = map;
		cellnum = 0;
		Cell cell2;
		FileInputStream file = new FileInputStream(new File(pathXLS));
		HSSFWorkbook workbook = new HSSFWorkbook(file);
		HSSFSheet sheetPCOD = workbook.getSheet("SOAP");

		rownum = sheetPCOD.getPhysicalNumberOfRows();
		//Integer.parseInt(GlobalData.getData("vLinha"));

		try {

			/**
			 *
			 * INSIRA AQUI AS VARIAVEIS DA ABA DADOS DO PROJETO *XPTO* (CHAVES)
			 *
			 */

			row = sheetPCOD.createRow(rownum); // linha
			cell2 = row.createCell(cellnum++);// Coluna
			cell2.setCellValue((String) GlobalData.getData("vCPF"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("- 001 BLACKLIST - "));
			cell2 = row.createCell(cellnum++);


			file.close();

			FileOutputStream outFile = new FileOutputStream(new File(pathXLS));
			workbook.write(outFile);
			outFile.close();
			System.out.println("Arquivo Excel editado com sucesso!");
			System.out.println("///////////////////////////////////////////////////////////////");


		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo Excel não encontrado!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro na edição do arquivo!");
		}
	}

	public void criateXLSX() {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheetPCOD = workbook.createSheet("SOAP");

		row = sheetPCOD.createRow(rownum++);
		cell = row.createCell(cellnum++);
		cell.setCellValue("CPF");
		cell = row.createCell(cellnum++);
		cell.setCellValue("- 001 BLACKLIST - ");
		cell = row.createCell(cellnum++);

		/**
		 *
		 * INSIRA AQUI AS VARIAVEIS DA ABA DADOS DO PROJETO *XPTO* (VALORES)
		 *
		 **/


		try {
			FileOutputStream out = new FileOutputStream(new File(pathXLS));
			workbook.write(out);
			out.close();
			System.out.println("Arquivo Excel criado com sucesso (CPF - Yahsat)!");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo não encontrado!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro na edição do arquivo!");
		}
	}

}
