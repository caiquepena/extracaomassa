package com.serasa.test.utils;

import com.everis.GlobalData;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * EXPORT XMLS
 *
 * @author cfm2748
 *
 */
public class GestorXLSXSoapYahsatPJ {
	String pathXLS = "./massaDados/Relatório GESTOR Soap CNPJ Yahsat PJ.xls";
	int i = 0;
	int rownum = 0;
	int cellnum = 0;
	Cell cell;
	Row row;
	Map<String, String> dadosResumoDecisao = new HashMap<String, String>();

	public void addDadosXSLX(Map map) throws IOException {
		dadosResumoDecisao = map;
		cellnum = 0;
		Cell cell2;
		FileInputStream file = new FileInputStream(new File(pathXLS));
		HSSFWorkbook workbook = new HSSFWorkbook(file);
		HSSFSheet sheetPCOD = workbook.getSheet("SOAP");

		rownum = sheetPCOD.getPhysicalNumberOfRows();
		//Integer.parseInt(GlobalData.getData("vLinha"));

		try {

			row = sheetPCOD.createRow(rownum); // linha
			cell2 = row.createCell(cellnum++);// Coluna
			cell2.setCellValue((String) GlobalData.getData("vCPF"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("- 001 BLACKLIST - "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("- 003 BLACKLIST - "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("CEP NUMERICO"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DATA DE NASCIMENTO"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DATA DO SISTEMA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DATA ULTIMA ATUALIZACAO SERASA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DCTO ROUB, FURT, EXTRAV - DATA 1"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DCTO ROUB, FURT, EXTRAV - DATA 2"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DCTO ROUB, FURT, EXTRAV - DATA 3"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DCTO ROUB, FURT, EXTRAV - DATA 4"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("DCTO ROUB, FURT, EXTRAV - DATA 5"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("ESTADO ONDE RESIDE"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("GRAFIA DA MAE"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("GRAFIA DO TITULAR"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("OBITO - ANO DE OBITO AAAA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPACAO EM FALENCIA (PIE)"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("PONTUACAO DO SCORE"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES JUDICIAIS ATE A DATA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT CHEQUES S/ FUNDOS ATE A DATA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT CHEQUES SUSTADOS ATE A DATA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT CHEQUES SUSTADOS ULT 6M"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT PROTESTOS ATE A DATA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QT PROTESTOS ULT 12M"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QTDE DIVIDA VENCIDA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QTDE PEFIN ULT 12 MESES"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QTDE PENDENCIAS FINANCEIRAS(PEFIN)"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QTDE PENDENCIAS FINANCEIRAS(REFIN)"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QTDE REFIN ULT 12 MESES"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QUANTIDADE DE CONSULTAS A CREDITO DO MES ATUAL -1"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QUANTIDADE DE CONSULTAS A CREDITO DO MES ATUAL -2"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("QUANTIDADE DE CONSULTAS A CREDITO DO MES ATUAL -3"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RF - DATA ATUALIZACAO"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RF - SITUACAO CADASTRAL"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("SITUACAO DO CPF"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VALR PEFIN ULT 12 MESES"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VALR REFIN ULT 12 MESES"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VLR PENDENCIAS FINANCEIRAS(PEFIN)"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VLR PENDENCIAS FINANCEIRAS(REFIN)"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VR ACOES JUDICIAIS ATE A DATA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VR CHEQUES SEM FUNDOS ATE A DATA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VR DIVIDA VENCIDA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("VR PROTESTOS ATE A DATA"));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - DATA DE NASCIMENTO ANOS - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - QTD CONSULTAS 3 MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - CESTA EVENTOS - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_FF - TEMPO ATUALIZACAO REC FED - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - FS - NOME COMPLETO - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - FS - NOME DA MAE - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - PEFINREFINPROTDIVVENMENOR12MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - VLR PEFINMENOR12MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - VLR REFINMENOR12MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - VLR PROTESTOMENOR12MESE - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - QTDPEFINREFINPROTDIVVENMAIO12MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - CALCULO DIVIDAS - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - PEFINREFINPROTDIVVENMAIOR12MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - VLR PEFINMAIOR12MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - VLR REFINMAIOR12MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - VLR PROTESTOMAIOR12MESES - CB" ));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - CESTA RESTRITIVOS - QTDE - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - CESTA RESTRITIVOS - VALOR - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - DATA ULTIMA ATUALIZACAO SERASA MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("FUN_YAHS - CPF POSSUI CONSULTA ULT 3 MESES - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - MOTIVO REPROVACAO CPF - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - CLASSIFICACAO DO SCORE POR UF - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - PONTUACAO APROVADO CPF - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - PONTUACAO PENDENTE CPF - CB "));
			cell2 = row.createCell(cellnum++);
			cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - DETAIL_CONS1_1036 - CB "));



			file.close();

			FileOutputStream outFile = new FileOutputStream(new File(pathXLS));
			workbook.write(outFile);
			outFile.close();
			System.out.println("Arquivo Excel editado com sucesso!");
			System.out.println("///////////////////////////////////////////////////////////////");


		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo Excel não encontrado!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro na edição do arquivo!");
		}
	}

	public void criateXLSX() {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheetPCOD = workbook.createSheet("SOAP");

		row = sheetPCOD.createRow(rownum++);
		cell = row.createCell(cellnum++);
		cell.setCellValue("CPF");
		cell = row.createCell(cellnum++);
		cell.setCellValue("- 001 BLACKLIST - ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("- 003 BLACKLIST - ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("CEP NUMERICO");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DATA DE NASCIMENTO");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DATA DO SISTEMA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DATA ULTIMA ATUALIZACAO SERASA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DCTO ROUB, FURT, EXTRAV - DATA 1");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DCTO ROUB, FURT, EXTRAV - DATA 2");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DCTO ROUB, FURT, EXTRAV - DATA 3");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DCTO ROUB, FURT, EXTRAV - DATA 4");
		cell = row.createCell(cellnum++);
		cell.setCellValue("DCTO ROUB, FURT, EXTRAV - DATA 5");
		cell = row.createCell(cellnum++);
		cell.setCellValue("ESTADO ONDE RESIDE");
		cell = row.createCell(cellnum++);
		cell.setCellValue("GRAFIA DA MAE");
		cell = row.createCell(cellnum++);
		cell.setCellValue("GRAFIA DO TITULAR");
		cell = row.createCell(cellnum++);
		cell.setCellValue("OBITO - ANO DE OBITO AAAA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("PARTICIPACAO EM FALENCIA (PIE)");
		cell = row.createCell(cellnum++);
		cell.setCellValue("PONTUACAO DO SCORE");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QT ACOES JUDICIAIS ATE A DATA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QT CHEQUES S/ FUNDOS ATE A DATA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QT CHEQUES SUSTADOS ATE A DATA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QT CHEQUES SUSTADOS ULT 6M");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QT PROTESTOS ATE A DATA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QT PROTESTOS ULT 12M");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QTDE DIVIDA VENCIDA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QTDE PEFIN ULT 12 MESES");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QTDE PENDENCIAS FINANCEIRAS(PEFIN)");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QTDE PENDENCIAS FINANCEIRAS(REFIN)");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QTDE REFIN ULT 12 MESES");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QUANTIDADE DE CONSULTAS A CREDITO DO MES ATUAL -1");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QUANTIDADE DE CONSULTAS A CREDITO DO MES ATUAL -2");
		cell = row.createCell(cellnum++);
		cell.setCellValue("QUANTIDADE DE CONSULTAS A CREDITO DO MES ATUAL -3");
		cell = row.createCell(cellnum++);
		cell.setCellValue("RF - DATA ATUALIZACAO");
		cell = row.createCell(cellnum++);
		cell.setCellValue("RF - SITUACAO CADASTRAL");
		cell = row.createCell(cellnum++);
		cell.setCellValue("SITUACAO DO CPF");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VALR PEFIN ULT 12 MESES");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VALR REFIN ULT 12 MESES");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VLR PENDENCIAS FINANCEIRAS(PEFIN)");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VLR PENDENCIAS FINANCEIRAS(REFIN)");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VR ACOES JUDICIAIS ATE A DATA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VR CHEQUES SEM FUNDOS ATE A DATA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VR DIVIDA VENCIDA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("VR PROTESTOS ATE A DATA");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - DATA DE NASCIMENTO ANOS - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - QTD CONSULTAS 3 MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - CESTA EVENTOS - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_FF - TEMPO ATUALIZACAO REC FED - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - FS - NOME COMPLETO - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - FS - NOME DA MAE - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - PEFINREFINPROTDIVVENMENOR12MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - VLR PEFINMENOR12MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - VLR REFINMENOR12MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - VLR PROTESTOMENOR12MESE - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - QTDPEFINREFINPROTDIVVENMAIO12MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - CALCULO DIVIDAS - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - PEFINREFINPROTDIVVENMAIOR12MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - VLR PEFINMAIOR12MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - VLR REFINMAIOR12MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - VLR PROTESTOMAIOR12MESES - CB" );
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - CESTA RESTRITIVOS - QTDE - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - CESTA RESTRITIVOS - VALOR - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - DATA ULTIMA ATUALIZACAO SERASA MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("FUN_YAHS - CPF POSSUI CONSULTA ULT 3 MESES - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("RP_RP - MOTIVO REPROVACAO CPF - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("RP_RP - CLASSIFICACAO DO SCORE POR UF - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("RP_RP - PONTUACAO APROVADO CPF - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("RP_RP - PONTUACAO PENDENTE CPF - CB ");
		cell = row.createCell(cellnum++);
		cell.setCellValue("RP_RP - DETAIL_CONS1_1036 - CB ");


		try {
			FileOutputStream out = new FileOutputStream(new File(pathXLS));
			workbook.write(out);
			out.close();
			System.out.println("Arquivo Excel criado com sucesso (CPF - Yahsat)!");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo não encontrado!");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Erro na edição do arquivo!");
		}
	}

}
