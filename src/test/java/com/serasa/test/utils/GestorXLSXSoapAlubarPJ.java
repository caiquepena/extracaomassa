package com.serasa.test.utils;

import com.everis.GlobalData;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * EXPORT XMLS
 *
 * @author cfm2748
 */
public class GestorXLSXSoapAlubarPJ {
    String pathXLS = "./massaDados/Relatório GESTOR Soap Alubar PJ.xls";
    int i = 0;
    int rownum = 0;
    int cellnum = 0;
    Cell cell;
    Row row;

    Map<String, String> dadosResumoDecisao = new HashMap<String, String>();

    public void addDadosXSLX(Map map) throws IOException {
        dadosResumoDecisao = map;
        cellnum = 0;
        Cell cell2;
        FileInputStream file = new FileInputStream(new File(pathXLS));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheetPCOD = workbook.getSheet("SOAP");

        rownum = sheetPCOD.getPhysicalNumberOfRows();

        try {

            row = sheetPCOD.createRow(rownum); // linha
            cell2 = row.createCell(cellnum++);// Coluna
            cell2.setCellValue((String) GlobalData.getData("vCNPJ"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("% PAGTOS PONTUAIS"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("ALERTA (S / N)"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("AUTO-FALENCIA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("CONCORDATA DEFERIDA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("CONCORDATA REQUERIDA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("CONCORDATA SUSPENSIVA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("DATA DA ENTRADA DO SOCIO MAIS RECENTE"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("DATA DO SISTEMA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("EXTINCAO DE FALENCIA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("FALENCIA DECRETADA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("FALENCIA REQUERIDA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPACAO EM FALENCIA (PIE)"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("PARTICIPACAO EM FALENCIA (PIE) SOCIOS"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("PORTE DA EMPRESA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES BUSCA E APREENSAO COL ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES BUSCA E APREENSAO EMP ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES EXEC FISCAL ESTADUAL EMP ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES EXEC FISCAL FEDERAL EMP ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES EXEC FISCAL MUNICIPAL EMP ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES EXECUCAO ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES EXECUCAO SOC ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES JUDICIAIS ATE A DATA "));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT ACOES JUDICIAIS SOC ATE A DATA "));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT CHEQUES S/FUNDO ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT CHEQUES S/FUNDO SOCIOS ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT PEFIN-EMPRESAS ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT PEFIN-SOCIOS ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT PROTESTOS ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT PROTESTOS SOCIOS ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT REFIN-BANCOS ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QT REFIN-SOCIOS ATE A DATA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QUANTIDADE DE CHEQUES EXTRAVIADOS"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("QUANTIDADE DE CHEQUES SUSTADOS"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("RECUPERACAO EXTRA-JUDICIAL HOMOLOGADA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("RECUPERACAO EXTRA-JUDICIAL REQUERIDA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("RECUPERACAO JUDICIAL CONCEDIDA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("RECUPERACAO JUDICIAL REQUERIDA"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("STATUS DO CNPJ"));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("FUN_FF - ALUBAR - DATA DE ENTRADA DOS SOCIOS - RC "));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - ALUBAR - RESTRITIVO EMPRESA - RC "));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - ALUBAR - RESTRICAO SOCIOS - RC "));
            cell2 = row.createCell(cellnum++);
            cell2.setCellValue((String) dadosResumoDecisao.get("RP_RP - ALUBAR - AÇÕES E EXECUCAO JURDICIAL - RC "));

            file.close();

            FileOutputStream outFile = new FileOutputStream(new File(pathXLS));
            workbook.write(outFile);
            outFile.close();
            System.out.println("Arquivo Excel editado com sucesso!");
            System.out.println("///////////////////////////////////////////////////////////////");


        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Arquivo Excel não encontrado!");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erro na edição do arquivo!");
        }
    }

    public void criateXLSX() {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheetPCOD = workbook.createSheet("SOAP");


        row = sheetPCOD.createRow(rownum++);
        cell = row.createCell(cellnum++);
        cell.setCellValue("CNPJ");
        cell = row.createCell(cellnum++);
        cell.setCellValue("% PAGTOS PONTUAIS");
        cell = row.createCell(cellnum++);
        cell.setCellValue("ALERTA (S / N)");
        cell = row.createCell(cellnum++);
        cell.setCellValue("AUTO-FALENCIA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("CONCORDATA DEFERIDA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("CONCORDATA REQUERIDA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("CONCORDATA SUSPENSIVA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("DATA DA ENTRADA DO SOCIO MAIS RECENTE");
        cell = row.createCell(cellnum++);
        cell.setCellValue("DATA DO SISTEMA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("EXTINCAO DE FALENCIA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("FALENCIA DECRETADA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("FALENCIA REQUERIDA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("PARTICIPACAO EM FALENCIA (PIE)");
        cell = row.createCell(cellnum++);
        cell.setCellValue("PARTICIPACAO EM FALENCIA (PIE) SOCIOS");
        cell = row.createCell(cellnum++);
        cell.setCellValue("PORTE DA EMPRESA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES BUSCA E APREENSAO COL ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES BUSCA E APREENSAO EMP ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES EXEC FISCAL ESTADUAL EMP ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES EXEC FISCAL FEDERAL EMP ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES EXEC FISCAL MUNICIPAL EMP ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES EXECUCAO ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES EXECUCAO SOC ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES JUDICIAIS ATE A DATA ");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT ACOES JUDICIAIS SOC ATE A DATA ");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT CHEQUES S/FUNDO ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT CHEQUES S/FUNDO SOCIOS ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT PEFIN-EMPRESAS ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT PEFIN-SOCIOS ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT PROTESTOS ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT PROTESTOS SOCIOS ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT REFIN-BANCOS ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QT REFIN-SOCIOS ATE A DATA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QUANTIDADE DE CHEQUES EXTRAVIADOS");
        cell = row.createCell(cellnum++);
        cell.setCellValue("QUANTIDADE DE CHEQUES SUSTADOS");
        cell = row.createCell(cellnum++);
        cell.setCellValue("RECUPERACAO EXTRA-JUDICIAL HOMOLOGADA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("RECUPERACAO EXTRA-JUDICIAL REQUERIDA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("RECUPERACAO JUDICIAL CONCEDIDA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("RECUPERACAO JUDICIAL REQUERIDA");
        cell = row.createCell(cellnum++);
        cell.setCellValue("STATUS DO CNPJ");
        cell = row.createCell(cellnum++);
        cell.setCellValue("FUN_FF - ALUBAR - DATA DE ENTRADA DOS SOCIOS - RC ");
        cell = row.createCell(cellnum++);
        cell.setCellValue("RP_RP - ALUBAR - RESTRITIVO EMPRESA - RC ");
        cell = row.createCell(cellnum++);
        cell.setCellValue("RP_RP - ALUBAR - RESTRICAO SOCIOS - RC ");
        cell = row.createCell(cellnum++);
        cell.setCellValue("RP - ALUBAR - AÇÕES E EXECUCAO JURDICIAL - RC ");
        cell.setCellValue(" TIPO_DEC = Avaliar");


        try {

            FileOutputStream out = new FileOutputStream(new File(pathXLS));
            workbook.write(out);
            out.close();
            System.out.println("Arquivo Excel criado com sucesso (Alubar PJ)!");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Arquivo não encontrado!");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erro na edição do arquivo!");
        }
    }

}
