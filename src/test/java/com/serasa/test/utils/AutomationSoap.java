package com.serasa.test.utils;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.rules.TestName;

import com.everis.Action;
import com.everis.EFA;
import com.everis.ExecutionInfo;
import com.everis.Manager;
import com.everis.data.DataLoad;
	
@Ignore
public class AutomationSoap {
	
	static GestorXLSXSoapRelato writeXLSXRelato;
	static GestorXLSXSoapABCRelato writeXLSXABCRelato;
	static GestorXLSXSoapRisk	writeXLSXRisk;
	static GestorXLSXSoapRating writeXLSXRating;
	static GestorXLSXSoapCpf writeXLSXCpf;
	static GestorXLSXSoapABCRating writeXLSXABCRating;
	static GestorXLSXSoapKlabinRisk writeXLSXKlabinRisk;
	static GestorXLSXSoapYahsatPFCSBA writeXLSXCpfYahsat ;
	static GestorXLSXSoapMexichemPJ writeXLXSMexichemPJ;
	static GestorXLSXSoapAlubarPJ writeXLXSAlubarPJ;
	static GestorXLSXSoapNovoProjetoSegmento writeXLXSNovoProjetoSegmento;
	// private static CoreWeb web;

	@Rule
	public TestName currentTest = new TestName();

	/**
	 * Load data from input file or data base
	 * 
	 * @return List with data
	 * @throws Exception
	 */
	public static List<Object> loadData() throws Exception {
		// Capture test name and test suite
		String[] fullClassName = new Throwable().getStackTrace()[1].getClassName().toString().split("\\W");
		ExecutionInfo.setTestSuite(fullClassName[fullClassName.length - 2]);
		ExecutionInfo.setTestName(fullClassName[fullClassName.length - 1]);
		// Load data used on script
		return Arrays.asList(new DataLoad().load());
	}

	/**
	 * Initialize the framework
	 * 
	 * @throws Exception
	 */
	@BeforeClass
	public static void beforeExecution() throws Exception {
		writeXLSXRelato = new GestorXLSXSoapRelato();
		writeXLSXRisk	= new GestorXLSXSoapRisk();
		writeXLSXRating = new GestorXLSXSoapRating();
		writeXLSXCpf	= new GestorXLSXSoapCpf();
		writeXLSXABCRelato = new GestorXLSXSoapABCRelato();
		writeXLSXABCRating = new GestorXLSXSoapABCRating();
		writeXLSXKlabinRisk = new GestorXLSXSoapKlabinRisk();
		writeXLSXCpfYahsat =  new GestorXLSXSoapYahsatPFCSBA();
		writeXLXSMexichemPJ = new GestorXLSXSoapMexichemPJ();
		writeXLXSAlubarPJ = new GestorXLSXSoapAlubarPJ();
		writeXLXSNovoProjetoSegmento = new GestorXLSXSoapNovoProjetoSegmento();

		writeXLXSMexichemPJ.criateXLSX();
		writeXLSXRelato.criateXLSX();
		writeXLSXRisk.criateXLSX();
		writeXLSXRating.criateXLSX();
		writeXLSXCpf.criateXLSX();
		writeXLSXABCRelato.criateXLSX();
		writeXLSXABCRating.criateXLSX();
		writeXLSXKlabinRisk.criateXLSX();
		writeXLSXCpfYahsat.criateXLSX();
		writeXLXSAlubarPJ.criateXLSX();
		writeXLXSNovoProjetoSegmento.criateXLSX();
	}

	/**
	 * Scenario - Postcondition for the test
	 * 
	 * @throws Exception
	 */
	@After
	public void afterRunTest() throws Exception {

	}

	/**
	 * Execution - Postcondition for the execution
	 * 
	 * @throws Exception
	 */
	@AfterClass
	public static void afterExecution() throws Exception {
	}

}
