package som.path.generated;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.junit.Test;

public class WsgestordecisaoSoapTest {

    public static Map<String, String> dadosAnaliseCredito = new HashMap<String, String>();
    String resultado;
    String[] textoSeparado1;
    String[] temp2;
    Map<String, String> textoSeparado2 = new HashMap<String, String>();

    @Test
    public void test() throws InterruptedException {
        getDados("00482277000106", 1);
        splitString1();
        System.out.println("********************************************************");
        dadosAnaliseCredito = dadosSoap();
        System.out.println(dadosAnaliseCredito);
    }

    public void getDados(String cnpj, int number) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("23321123000120", "HOMOLOGA", "GESTOR@1", "99221291", "GESTOR@8",
                cnpj, 0, "", true, false, "tipoconsulta@" + number + "");
        System.out.println(resultado);
    }

    public void getDadosABCRelato(String cnpj, int number) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("28195667000106", "EXTRACAO", "serasa", "79020111", "GESTOR@1",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosABCRating(String cnpj, int number) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("28195667000106", "TESTE_CR", "serasa", "79020111", "GESTOR@1",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }


    public void getDadosCpf(String cpf) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("23321123000120", "HOMOLOGA", "GESTOR@1", "99221291", "GESTOR@8",
                cpf, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosCpfYahsat(String cpf) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("20854761000118", "EXTRACAO", "serasa", "39276274", "10203040",
                cpf, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosMexichemPJ(String cnpj) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("58514928000174", "TESTEQA", "SERASA", "47062463", "Gestor@1",
                cnpj, 0, "", true, false, "DO - GRADE DE CLIENTE@0");
        System.out.println(resultado);
    }

    public void getDadosAlubarPJ(String cnpj) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("08262121000113", "SOAP", "SERASA", "23593958", "Gestor@1",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosDarkaPJ(String cnpj) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("01887122000104", "TESTE", "serasa", "39276274", "10203040",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosCanonPJ(String cnpj) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
                this.resultado = serv.analisarCredito("46266771000126", "TESTEQA", "serasa", "84264033", "Gestor@1",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosYahsatPJ(String cnpj) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("20854761000118", "EXTRACAO", "serasa", "39276274", "10203040",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosSwissRePJ(String cnpj) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("72145931000199", "SOAP", "Serasa", "50883604", "Gestor@1",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }

    public void getDadosNovoProjetoSegmento(String cnpj) {
        WsgestordecisaoSoap serv = new Wsgestordecisao().getWsgestordecisaoSoap();
        this.resultado = serv.analisarCredito("CNPJCLIENTE", "USUARIOGESTOR", "SENHAGESTOR", "USUARIOSERASA", "SENHASERASA",
                cnpj, 0, "", true, false, "");
        System.out.println(resultado);
    }




    public void splitString1() {
        String temp;
        this.textoSeparado1 = resultado.split("\\|");
        temp = textoSeparado1[0].replaceAll("DADOSPOLITICA = ", "");
        this.textoSeparado1[0] = temp;
        System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
    }

    public Map dadosSoap() throws InterruptedException {
        String temp;
        Map<String, String> dadosTemp = new HashMap<String, String>();

        int numberOfItems = textoSeparado1.length;
        for (int counter = 0; counter < numberOfItems - 1; counter++) {
            temp = textoSeparado1[counter];
            if (temp.contains("@")) {
                temp2 = temp.split("\\@");
                dadosTemp.put(temp2[0], temp2[1]);
            } else {
            }
        }
        return dadosTemp;
    }
}

